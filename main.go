package main

import (
	"github.com/d2r2/go-bsbmp"
	"github.com/d2r2/go-i2c"
	logger "github.com/d2r2/go-logger"
	"time"

	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var lg = logger.NewPackageLogger("main",
	logger.DebugLevel,
	// logger.InfoLevel,
)

type envoirment struct
{
	temperature float32
	pressure float32
	humidity float32
	measuretime int64
}

func main() {
	defer logger.FinalizeLogger()
	// Create new connection to i2c-bus on 1 line with address 0x76.
	// Use i2cdetect utility to find device address over the i2c-bus
	i2c, err := i2c.NewI2C(0x76, 0)
	if err != nil {
		lg.Fatal(err)
	}
	defer i2c.Close()

	lg.Notify("***************************************************************************************************")
	lg.Notify("*** You can change verbosity of output, to modify logging level of modules \"i2c\", \"bsbmp\"")
	lg.Notify("*** Uncomment/comment corresponding lines with call to ChangePackageLogLevel(...)")
	lg.Notify("***************************************************************************************************")
	// Uncomment/comment next lines to suppress/increase verbosity of output
	logger.ChangePackageLogLevel("i2c", logger.InfoLevel)
	logger.ChangePackageLogLevel("bsbmp", logger.InfoLevel)

	sensor, err := bsbmp.NewBMP(bsbmp.BME280, i2c) // signature=0x60
	if err != nil {
		lg.Fatal(err)
	}

	id, err := sensor.ReadSensorID()
	if err != nil {
		lg.Fatal(err)
	}
	lg.Infof("This Bosch Sensortec sensor has signature: 0x%x", id)

	err = sensor.IsValidCoefficients()
	if err != nil {
		lg.Fatal(err)
	}

	auth := options.Credential{AuthMechanism: "SCRAM-SHA-1", Username: "sensor", Password: "d8ac88ec4e0db331e096561199c45a206fafe4d28208992ad9e6058cbf58e51a", AuthSource: "env"}
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://54.37.138.245:27017"), options.Client().SetAuth(auth))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	databases, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(databases)
	//db := client.Database("env")
	//fmt.Println(db.Name())
	//coll := db.Collection("sw")
	//fmt.Println(coll.Name())
	coll := client.Database("env").Collection("sw")
	fmt.Println(coll.Name())

	for 
	{
			// Read temperature in celsius degree
		t, err := sensor.ReadTemperatureC(bsbmp.ACCURACY_ULTRA_HIGH)
		if err != nil {
			lg.Fatal(err)
		}
		lg.Infof("Temprature = %v*C", t)

		// Read atmospheric pressure in pascal
		p, err := sensor.ReadPressurePa(bsbmp.ACCURACY_ULTRA_HIGH)
		if err != nil {
			lg.Fatal(err)
		}
		lg.Infof("Pressure = %v Pa", p)

		// Read atmospheric pressure in mmHg
		//p, err = sensor.ReadPressureMmHg(bsbmp.ACCURACY_ULTRA_HIGH)
		//if err != nil {
		//	lg.Fatal(err)
		//}
		//lg.Infof("Pressure = %v mmHg", p)

		// Read atmospheric pressure in mmHg
		supported, h1, err := sensor.ReadHumidityRH(bsbmp.ACCURACY_ULTRA_HIGH)
		if supported {
			if err != nil {
				lg.Fatal(err)
			}
			lg.Infof("Humidity = %v %%", h1)
		}

		// Read atmospheric altitude in meters above sea level, if we assume
		// that pressure at see level is equal to 101325 Pa.
		//a, err := sensor.ReadAltitude(bsbmp.ACCURACY_LOW)
		//if err != nil {
		//	lg.Fatal(err)
		//}
		//lg.Infof("Altitude = %v m", a)
		tm := time.Now().UnixNano()
		lg.Infof("Time = %v", tm)

		result := envoirment{temperature: t,  pressure: p, humidity: h1, measuretime: tm}
		lg.Infof("Temprature = %v*C, Pressure = %v Pa, Humidity = %v %%, Time =%v", result.temperature, result.pressure, result.humidity, result.measuretime)
		time.Sleep(time.Second)

	}
	
}
